﻿using OpenQA.Selenium;

namespace OnlineStoreAutomationTest2.Page
{
    public class ProductDetail : CommonComponent
    {
        public ProductDetail(IWebDriver driver) : base(driver)
        {
        }
        public IWebElement SendToFriendLink => Driver.FindElement(By.Id("send_friend_button"));
        public IWebElement AddToCartButton => Driver.FindElement(By.Name("Submit"));
        public IWebElement ProceedToCheckoutButton => Driver.FindElement(By.XPath("//a[@title='Proceed to checkout']"));
        public IWebElement ContinueShoppingButton => Driver.FindElement(By.XPath("//span[@title='Continue shopping']"));
        public IWebElement FriendNameInput => Driver.FindElement(By.Id("friend_name"));
        public IWebElement FriendEmailInput => Driver.FindElement(By.Id("friend_email"));
        public IWebElement SendEmailButton => Driver.FindElement(By.Id("sendEmail"));
        public IWebElement ErrorMessage => Driver.FindElement(By.Id("send_friend_form_error"));
        public IWebElement SuccessModal => Driver.FindElement(By.XPath("//div[@class='fancybox-wrap fancybox-desktop fancybox-type-html fancybox-opened']"));

    }
}
