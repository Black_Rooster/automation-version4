﻿using OpenQA.Selenium;

namespace OnlineStoreAutomationTest2.Page
{
    public class Search : CommonComponent
    {
        public Search(IWebDriver driver) : base(driver)
        {
        }
        public IWebElement ProductLink => Driver.FindElement(By.XPath("//a[@title='Faded Short Sleeve T-shirts' and @class='product-name']"));
        public IWebElement AddToCartButton => ProductLink.FindElement(By.XPath("//a[@title='Add to cart']"));

        public void ProductHoverEffect()
        {
            Action.MoveToElement(ProductLink).Build().Perform();
        }
    }
}
