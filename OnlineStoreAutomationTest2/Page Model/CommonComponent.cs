﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;

namespace OnlineStoreAutomationTest2.Page
{
    public class CommonComponent
    {
        public IWebDriver Driver;
        public WebDriverWait DriverWait;
        public Actions Action;
        public string ProductName;

        public CommonComponent(IWebDriver driver)
        {
            Driver = driver;
            DriverWait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
            Action = new Actions(Driver);
        }

        public IWebElement SearchField => Driver.FindElement(By.Name("search_query"));
        public IWebElement SearchButton => Driver.FindElement(By.Name("submit_search"));

    }
}
