﻿using OpenQA.Selenium;

namespace OnlineStoreAutomationTest2.Page
{
    public class Cart : CommonComponent
    {
        public Cart(IWebDriver driver) : base(driver)
        {
        }
        public IWebElement ProductCounter => Driver.FindElement(By.ClassName("ajax_cart_no_product"));
        public IWebElement RemoveProduct => Driver.FindElement(By.XPath("//a[@title='Delete']"));
        public IWebElement CartProductLink => Driver.FindElement(By.LinkText("Faded Short Sleeve T-shirts"));
        public void WaitForUpdate()
        {
            DriverWait.Until(value => ProductCounter.Displayed == true);
        }
    }
}
