﻿using NUnit.Framework;

namespace OnlineStoreAutomationTest2.Test
{
    public class AddToCart : Main
    {
        private readonly string _productName = "Faded Short Sleeve T-shirts";

        [SetUp]
        public void Init()
        {
            StoreSetup.CommonComponent.SearchField.SendKeys(_productName);
            StoreSetup.CommonComponent.SearchButton.Click();
        }

        [Test]
        public void GivenProductName_WhenAdddingToCartWithinProductDetails_ThenAddToCart()
        {
            StoreSetup.Search.ProductLink.Click();
            StoreSetup.ProductDetail.AddToCartButton.Click();
            StoreSetup.ProductDetail.ProceedToCheckoutButton.Click();

            Assert.IsTrue(StoreSetup.Cart.CartProductLink.Displayed, "Product was not added");
        }

        [Test]
        public void GivenProductName_WhenAdddingToCartFromSearchResult_ThenAddToCart()
        {
            StoreSetup.Search.ProductHoverEffect();
            StoreSetup.Search.AddToCartButton.Click();
            StoreSetup.ProductDetail.ProceedToCheckoutButton.Click();

            Assert.IsTrue(StoreSetup.Cart.CartProductLink.Displayed, "Product was not added");
        }

        [Test]
        public void GivenProductName_WhenRemovingProductInTheCart_ThenRemoveProduct()
        {
            StoreSetup.Search.ProductLink.Click();
            StoreSetup.ProductDetail.AddToCartButton.Click();
            StoreSetup.ProductDetail.ProceedToCheckoutButton.Click();
            StoreSetup.Cart.RemoveProduct.Click();
            StoreSetup.Cart.WaitForUpdate();

            Assert.AreEqual("(empty)", StoreSetup.Cart.ProductCounter.GetAttribute("textContent"));
        }
    }
}
