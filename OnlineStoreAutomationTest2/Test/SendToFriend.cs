﻿using NUnit.Framework;

namespace OnlineStoreAutomationTest2.Test
{

    [TestFixture]
    class SendToFriend : Main
    {

        [SetUp]
        public void Init()
        {
            var _productName = "Faded Short Sleeve T-shirts";

            StoreSetup.CommonComponent.SearchField.SendKeys(_productName);
            StoreSetup.CommonComponent.SearchButton.Click();
            StoreSetup.Search.ProductLink.Click();
            StoreSetup.ProductDetail.SendToFriendLink.Click();
        }

        [Test]
        public void GivenValidInput_WhenSendingProductToAFriend_ThenDisplaySuccessModal()
        {
            StoreSetup.ProductDetail.FriendNameInput.SendKeys("senzo");
            StoreSetup.ProductDetail.FriendEmailInput.SendKeys("senzo@gmail.com");
            StoreSetup.ProductDetail.SendEmailButton.Click();

            Assert.IsTrue(StoreSetup.ProductDetail.SuccessModal.Displayed, "Product not sent to a friend");
        }

        [Test]
        public void GivenEmptyField_WhenSendingProductToAFriend_ThenShowErrorMessage()
        {
            StoreSetup.ProductDetail.SendEmailButton.Click();

            Assert.AreEqual("You did not fill required fields", StoreSetup.ProductDetail.ErrorMessage.Text);
        }
    }
}
