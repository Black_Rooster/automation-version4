﻿using OnlineStoreAutomationTest2.Page;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineStoreAutomationTest2
{
    public static class StoreSetup
    {
        public static Cart Cart;
        public static CommonComponent CommonComponent;
        public static ProductDetail ProductDetail;
        public static Search Search;

        public static void Initialize(IWebDriver driver)
        {
            driver.Navigate().GoToUrl("http://automationpractice.com/index.php");

            CommonComponent = new CommonComponent(driver);
            Cart = new Cart(driver);
            ProductDetail = new ProductDetail(driver);
            Search = new Search(driver);
        }
    }
}
